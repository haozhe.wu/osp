# ops-client-js@0.0.1

ops-client-js is a ops clent js sdk.

## Installation

Use the package manager [pnpm] or [npm] to install  ops-client-js.

```bash
pnpm add ops-client-js
```
or

```bash
npm i ops-client-js --save-dev
```

## Usage

```python
import { useAuthLogin, useAuthChallenge, useAuthRefresh, useAuthVerify } from 'osp-client-js'

const { getChallenge } = useAuthChallenge()
const { postAuthLogin } = useAuthLogin()
const { postAuthVerify } = useAuthVerify()
const { postAuthRefresh } = useAuthRefresh()

const handleFetchChallenge = async()=>{
    const {err, data} =  await getChallenge({address})
    console.log('ret', err, data)
}
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
export async function signedTypeData(client: 
  { web3: 
    { 
      currentProvider: { sendAsync: (arg0: any, arg1: (err: any, data: any) => void) => void; 
    }; }; }, 
  body: any){
  return new Promise((resolve, reject) => {
    client.web3.currentProvider.sendAsync(body,(err, data)=>{
      console.log(data.result)
      resolve(data.result)
    })
  });

}


import { Client,DefaultGenerics } from './client'
import { AddActivityRequest, AddReactionRequest, FollowRequest } from './rest_api_generated';

export class TypeData<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
  }

  async activityCreate(
    data: AddActivityRequest
  ) {
    const res = await this.client.request.typedata.activityCreate(data)
    return res
  }
  async relationCreate(data: FollowRequest) {
    const res = await this.client.request.typedata.relationCreate(data)
    return res
  }
  async reactionCreate(data: AddReactionRequest) {
    const res = await this.client.request.typedata.reactionCreate(data)
    return res
  }
}
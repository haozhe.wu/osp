import { 
  CollectModule,
  ReferenceModule,
  ActivityCategoryEnum,
  ActivityRankingEnum,
} from './rest_api_generated'
import { Client,DefaultGenerics } from './client'
import { ethers } from 'ethers'

export type FeedData= {}

export type CollectType= {}

export type ReferenceType= {}

export type GetFeedOptions= {
  tenants?: any[]
  ranking: ActivityRankingEnum
  randomize?: boolean
  categories: ActivityCategoryEnum[]
  limit?: number,
  nextToken?: string,
}

export class Activity<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics> {
  client: Client<StreamFeedGenerics>
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client
  }

  /**
   * addActivity
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  async addActivity({ profileId,collect,reference,metadata, isOnChain }:{
    profileId: string,
    isOnChain?: boolean,
    collect?: CollectModule,
    reference?: ReferenceModule,
    metadata?: any,
  }) {
    const content_uri = await this.client.ipfs.upload(metadata)
    const res = await this.client.request.activities.addActivity({
        profile_id: profileId,
        content_uri,
        collect_module: collect,
        reference_module: reference
      },{ headers:{ 'On-Chain': `${!!isOnChain}` }})

    return res
  }

  async addActivityBroadCast({ profileId,collect,reference,metadata }:{
    profileId: string,
    // data?: FeedData,
    collect?: CollectModule,
    reference?: ReferenceModule,
    metadata?: any,
  }) {

    //upload to ipfs
    const content_uri = await this.client.ipfs.upload(metadata)

    const profile_id =  profileId || '0xf4'


    console.log('content_uri', content_uri)
    const body = {
          profile_id,
          content_uri,
          collect_module: collect,
          reference_module: reference
      }

    const res = await this.client.typedata.activityCreate(body)
    if(res.error) return { error: res.error }
    const type_data = res.data.obj.type_data
    const broadcastId = res.data.obj.broadcast_id

    var params = [this.client.address, JSON.stringify(type_data)];
    var method = 'eth_signTypedData_v4';
    let signature = ''
    if(typeof this.client.web3.currentProvider === "string") return
    const provider = this.client.web3.currentProvider
    if(!('sendAsync' in provider)) return
    provider?.sendAsync({
      method,
      params,
      // @ts-ignore
      from: this.client.address
    },(error: any, data: { result: string })=>{
      if(error) throw new Error(JSON.stringify(error))
      signature  = data.result
      const res =  this.client.broadcast.create(broadcastId , signature)
      return res

    })
  }

  async createActivityPostWithSig ({ profileId,collect,reference,metadata }:{
    profileId: string,
    collect?: CollectModule,
    reference?: ReferenceModule,
    metadata: any
  }){
      //upload to ipfs
      const content_uri = await this.client.ipfs.upload(metadata)

      const body = {
            profile_id: profileId,
            content_uri,
            collect_module: collect,
            reference_module: reference
        }
    const { data,error } = await this.client.typedata.activityCreate(body)
    if(error) return { error }
    const { obj:{ type_data, type_data:{ message } } } = data

    const params = [this.client.address, JSON.stringify(type_data)];
    const method = 'eth_signTypedData_v4';
    let signature = ''
    if(typeof this.client.web3.currentProvider === "string") return
    const provider = this.client.web3.currentProvider
    if(!('sendAsync' in provider)) return
    provider?.sendAsync({
      method,
      params,
      // @ts-ignore
      from: this.client.address
    },(err: any, data: { result: string })=>{
      if(err) throw new Error(JSON.stringify(error))
      signature  = data.result
      const { r, s, v } = ethers.utils.splitSignature(signature);
      this.client.contract.methods.postWithSig({
        profileId: message.profileId,
        contentURI: message.contentURI,
        collectModule: message.collectModule,
        collectModuleInitData:message.collectModuleInitData,
        referenceModule: message.referenceModule,
        referenceModuleInitData: message.referenceModuleInitData,
        sig:{
          v,
          r,
          s,
          deadline:message.deadline
        }
      }).send().then(() => {
        console.log('Transaction complete!');
      }).catch((error: string) => {
        console.error('Error occurred:', error);
        throw new Error(JSON.stringify(error))
        })
    })
    return { error: '' }
  }

  async createActivityPost ({ profileId,collect,reference,metadata }:{
    profileId: string,
    collect?: CollectModule,
    reference?: ReferenceModule,
    metadata: any
  }){
    //upload to ipfs
    const content_uri = await this.client.ipfs.upload(metadata)

    const body = {
          profile_id: profileId,
          content_uri,
          collect_module: collect,
          reference_module: reference
      }
    const { data,error } = await this.client.typedata.activityCreate(body)
    if(error) return { error }
    const { obj:{ type_data:{ message } } } = data
    this.client.contract.methods.post({
      profileId: message.profileId,
      contentURI: message.contentURI,
      collectModule: message.collectModule,
      collectModuleInitData:message.collectModuleInitData,
      referenceModule: message.referenceModule,
      referenceModuleInitData: message.referenceModuleInitData,
    }).send().then(() => {
      console.log('Transaction complete!');
    }).catch((error: string) => {
      console.error('Error occurred:', error);
     throw new Error(JSON.stringify(error))
    })
    return { error: '' }
  }

  /**
   * Removes the activity by activityId or foreignId
   * @link https://getstream.io/activity-feeds/docs/node/adding_activities/?language=js#removing-activities
   * @method removeActivity
   * @memberof StreamFeed.prototype
   * @param  {string} activityOrActivityId Identifier of activity to remove
   * @return {Promise<APIResponse & { removed: string }>}
   * @example feed.removeActivity(activityId);
   * @example feed.removeActivity({'foreign_id': foreignId});
   */
  async removeActivity(activityId: string) {
    const res = await this.client.request.activities.stopActivity(activityId)
    return res
  }

  /**
   * Reads the feed
   * @link https://getstream.io/activity-feeds/docs/node/adding_activities/?language=js#retrieving-activities
   * @method get
   * @memberof StreamFeed.prototype
   * @param {GetFeedOptions} options  Additional options
   * @return {Promise<FeedAPIResponse>}
   * @example feed.get({limit: 10, id_lte: 'activity-id'})
   * @example feed.get({limit: 10, mark_seen: true})
   */
  async get(options: GetFeedOptions = {
    categories: [],
    ranking: ActivityRankingEnum.ACTIVITY_TIME,
  }) {
    const res = await this.client.request.activities.getActivities({
        "limit": options?.limit || 20,
        categories: [ActivityCategoryEnum.POST],
        // "next_token": options?.nextToken || "",
        // "exclude_user_ids": [],
        // "randomize": options?.randomize || true,
        // "ranking": options?.ranking,
        // "tenants": options?.tenants || [],
        // "timestamp": 0
    })
    return res
  }

  /**
   * Retrieves one activity from a feed and adds enrichment
   * @link https://getstream.io/activity-feeds/docs/node/adding_activities/?language=js#retrieving-activities
   * @method getActivityDetail
   * @memberof StreamFeed.prototype
   * @param  {string}   activityId Identifier of activity to retrieve
   * @param  {EnrichOptions}   options  Additional options
   * @return {Promise<FeedAPIResponse>}
   * @example feed.getActivityDetail(activityId)
   * @example feed.getActivityDetail(activityId, {withRecentReactions: true})
   * @example feed.getActivityDetail(activityId, {withReactionCounts: true})
   * @example feed.getActivityDetail(activityId, {withOwnReactions: true, withReactionCounts: true})
   */
  async getActivityDetail(activityId: string) {
    const res = await this.client.request.activities.getActivity(activityId)
    return res
  }
}
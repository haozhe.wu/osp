import { Client,DefaultGenerics } from './client'
import { FollowModuleEnum,FollowModule } from './rest_api_generated';
export class User<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics> {
  client: Client<StreamFeedGenerics>;
  data: any;
  userId: any;
  profileId: any;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
    this.data = undefined;
    this.setCurUserInfo();
  }

  setCurUserInfo() {
    const user = this.client.cache?.getItem('sdk:user')
    let userObj = {} as any;
    try {
      userObj = JSON.parse(user)
    }catch (err){}
    const curUserKey = Object.keys(userObj)[0] || '';
    if(curUserKey){
      this.userId = userObj[curUserKey].id
      this.profileId = userObj[curUserKey].profile_id
    }
  }


  /**
   * Delete the user
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#removing-users
   * @return {Promise<APIResponse>}
   */
  async delete(userId: string) {
    const res = await this.client.request.delete(userId);
    return res
  }
  /**
   * Get the user profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async get(userId: string) {
    const { data,error } = await this.client.request.users.getUser(userId)
    if(error) return
    return data;
  }

  /**
   * Get the user all profile
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#retrieving-users
   * @param {boolean} [options.with_follow_counts]
   * @return {Promise<StreamUser>}
   */
  async getAllProfile() {
    const { data,error } = await this.client.request.users.getUsers({ address: this.client.address })
    if(error) return 
    // console.log(res,'resres')
    const rows = data.obj.rows
    if(this.client.enableCache){
      const obj = {} as any;
      rows.forEach((row) => {
        const { profile_id } = row
        obj[profile_id] = row
      })
      this.client.cache.setItem("sdk:user", JSON.stringify(obj));
      this.setCurUserInfo();
    }
    return rows;
  }


  /**
   * Create a new user in stream
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#adding-users
   * @param {object} data user date stored in stream
   * @param {boolean} [options.get_or_create] if user already exists return it
   * @return {Promise<StreamUser>}
   */
  async create(data: { 
    follow_module?: FollowModule; 
    follow_nft_uri?: string; handle?: string; bio?: string; avatar?: string; cover_picture?: string; }) {
    const res = await this.client.request.users.createUser(data);


    return res;
  }

  /**
   * Update a new user in stream
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#adding-users
   * @param {object} data user date stored in stream
   * @param {boolean} [options.get_or_create] if user already exists return it
   * @return {Promise<StreamUser>}
   */
  async update(userId: string, key: string, value: string) {
    const res = await this.client.request.users.updateUser(userId, {key, value});

    return res;
  }

  /**
   * Update a new user in stream
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#adding-users
   * @param {object} data user date stored in stream
   * @param {boolean} [options.get_or_create] if user already exists return it
   * @return {Promise<StreamUser>}
   */
  async setDispatcher(userId: string, address?: string, canRelay?: boolean) {
    const res = await this.client.request.users.setDispatcher(userId,{ dispatcher: {address, canRelay} });
    return res;
  }

  /**
   * Update a new user in stream
   * @link https://getstream.io/activity-feeds/docs/node/users_introduction/?language=js#adding-users
   * @param {object} data user date stored in stream
   * @param {boolean} [options.get_or_create] if user already exists return it
   * @return {Promise<StreamUser>}
   */
  async updateDispatcher(userId: string, enable?: boolean) {
    const res = await this.client.request.users.updateDispatcher(userId, {enable});

    return res;
  }
  


}
import { Client,DefaultGenerics } from './client'

export class Auth<StreamFeedGenerics extends DefaultGenerics = DefaultGenerics> {
  client: Client<StreamFeedGenerics>;
  constructor(client: Client<StreamFeedGenerics>) {
    this.client = client;
    const authToken = client.cache?.getItem('sdk:authToken')
    if(authToken){
      this.setReqHeaderToken(authToken)
    }
  }

  /**
   * signIn
   * @link https://getstream.io/activity-feeds/docs/node/reactions_introduction/?language=js#adding-reactions
   * @method add
   * @memberof StreamReaction.prototype
   * @param  {string}   kind  kind of reaction
   * @param  {string}   activity Activity or an ActivityID
   * @param  {ReactionType}   data  data related to reaction
   * @param  {ReactionAddOptions} [options]
   * @param  {string} [options.id] id associated with reaction
   * @param  {string[]} [options.targetFeeds] an array of feeds to which to send an activity with the reaction
   * @param  {string} [options.userId] useful for adding reaction with server token
   * @param  {object} [options.targetFeedsExtraData] extra data related to target feeds
   * @return {Promise<ReactionAPIResponse<ReactionType>>}
   * @example reactions.add("like", "0c7db91c-67f9-11e8-bcd9-fe00a9219401")
   * @example reactions.add("comment", "0c7db91c-67f9-11e8-bcd9-fe00a9219401", {"text": "love it!"},)
   */
  setReqHeaderToken(authToken: string){
    this.client.request.setSecurityData({
      headers: {
        Authorization: `Bearer ${authToken}`
      }
    })
  }
  async signIn(
  ) {

    //first get chanlange: https://docs.lens.xyz/docs/login#challenge
    const {error, data} = await this.client.request.auth.getChallenge({address: this.client.address}, {})
    // if(error){
    //   if(this.debv gug){
    //     //TODO, error log report
    //   }
    //   throw new Error(error)
    // }
    const challenage = data?.obj?.text

    //second Get signature
    console.log(this.client.web3,'kkkkkk')
    const signature = await this.client.web3.eth.personal.sign(
      challenage, 
      this.client.address,
      ''
    );
    console.log('signature', signature)

    // const signature = await signMessageAsync({
    //   message: challenage
    // });
    //third Auth user and set cookies
    const {error: errorAuth, data: dataAuth}: any = await this.client.request.auth.authenticate({ address: this.client.address, signature });
    console.log('auth', dataAuth, errorAuth)
    const authToken = dataAuth?.obj?.access_token
    this.setReqHeaderToken(authToken)
    if(this.client.enableCache){
      this.client.cache?.setItem('sdk:authToken', authToken)
    }
    // 登录成功之后，更新用户数据
    await this.client.user.getAllProfile();

    return {...dataAuth.obj }

  }
}
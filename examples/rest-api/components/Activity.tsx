import React from 'react';
import { useEffect, useState } from "react";

export const Activity = ({address, apiClient}:{address?: string, apiClient?: any})=>{
    
    const [activelist, setActivelist] = useState([])

    const createActivity = async()=>{
        await apiClient.activities.addActivity({
            // profile_id/userid
            profile_id: '0xf7',
            content_uri:'ipfs://QmYqrhL8Nzec4swV6bsHsv1ioojJUgRerszRsfeGz8VhhN',
        },{
            headers:{
                'On-Chain':false
            }
        })
    }
    const getAllActivities = async()=>{
        const { data,error } = await apiClient.activities.getActivities({
            limit: 10,
            "categories": [
                "POST"
              ],
              "randomize": true,
              "ranking": "ACTIVITY_TIME",
        })
        if(!error){
            setActivelist(data.obj.rows)
        }
    }

    const getActivityDetail = async(id)=>{
        const res = await apiClient.activities.getActivity(id)
        console.log("ppppp", res)
    }
    // 删除暂时不支持
    const deleteActivitiy = async(id)=>{
        const res = await apiClient.activities.stopActivity(id)
        console.log("ppppp", res)
    } 
    return (
         <div>
            <h2>Activity模块</h2>
            <h3>创建用户的Activity</h3>
            <button onClick={createActivity}>创建用户的Activity</button>
            <br />
            <h3>获取用户所有的Activity</h3>
            <button onClick={getAllActivities}>获取用户的所有Activity</button>
            <h4>listContent</h4>
            {
                activelist.map(i=> (
                    <div key={i.created} >
                        {i.text}
                        <button onClick={()=> getActivityDetail(i.view_id)}>查看详情</button>
                        <button onClick={()=> deleteActivitiy(i.view_id)}>删除</button>
                    </div>
                ))
            }
        </div>
    )
}
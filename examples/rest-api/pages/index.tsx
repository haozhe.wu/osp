import { getDefaultProvider } from 'ethers'
import { App } from '../widget/app/App'
import { WagmiConfig, createClient } from 'wagmi'

const client = createClient({
  autoConnect: true,
  // @ts-ignore
  provider: getDefaultProvider(),
})


export default function Example() {
  return (
    <WagmiConfig client={client}>
      <App />
    </WagmiConfig>
  )
}
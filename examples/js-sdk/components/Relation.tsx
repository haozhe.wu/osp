import React from 'react'
import { useState } from 'react'


const feedSlug = 'user';
// 关注人 不为 NullFollowModule时，需要转profileId —— 重复关注报错，需提供新userId
const userId = '0x127';

export function Relation({client}){
  // 关注人为 ProfileFollowModule
const body = {
  followModuleParam: {
    "type": "ProfileFollowModule",
    data: {
      profileId: client.user.profileId,
    }
  }
};
  const [followerList, setFollowerList] = useState([])
  const [followingList, setFollowingList] = useState([])

  const handleGetFollowingList = async () => {
    const feedSlug = 'user';
    const userId = client.user.userId;
    const query = {
      address: client.address,
      limit: 10,
    };
    const {err, data}: any  = await client.relation.following(feedSlug, userId, query);
    setFollowingList(data?.obj?.rows || [])
    console.log('handleGetFollowingList info', data, err)
  }

  const handleGetFollowerList = async () => {
    const feedSlug = 'user';
    const userId = client.user.userId;
    const query = {
      limit: 10,
    };
    const {err, data}: any  = await client.relation.followers(feedSlug, userId, query);
    setFollowerList(data?.obj?.rows || [])
    console.log('handleGetFollowerList info', data, err)
  }

  const handleDoFollow = async () => {
    const feedSlug = 'user';
    const userId = client.user.userId;
    const targetUserId = '51323505354768384'
    const body = {
      isOnChain: false,
      targetUserId
    };
    const {err, data}: any  = await client.relation.follow(feedSlug, userId, body);
    if (data?.code === 200) {
      alert('关注成功')
    }
    console.log('handleDoFollow info', data, err)
  }



  const handleDoFollowBroadCast = async () => {
    // 关注人为NullFollowModule，重复关注不报错
    // const userId = '0x11c';
    // const body = {
    //   followModuleParam: {
    //     "type": "NullFollowModule"
    //   }
    // };
    await client.relation.followBroadcast(feedSlug, userId, body);
  }

  const createRelationPostWithSig = async()=>{
    await client.relation.createRelationPostWithSig({
      userId,
      feedSlug,
      data: body
    })
  }
  const createRelationPost = async()=>{
    await client.relation.createRelationPost({
      userId,
      feedSlug,
      data: body
    })
  }

  return (
    <>
      <h2>Relation 模块</h2>
      <p>
        <h3>获取关注列表</h3>
        <button onClick={handleGetFollowingList}>getFollowingList</button>
        <p>
          {followingList.map((item, index) => {
            return <div key={`${item.owner}:${index}`}>{item.owner}{item.profile_id} --- {item.handle}</div>
          })}
        </p>
      </p>
      <p>
        <h3>获取粉丝列表</h3>
        <button onClick={handleGetFollowerList}>getFollowerList</button>
        <p>
          {followerList.map((item, index) => {
            return <div key={`${item.owner}:${index}`}>{item.owner}{item.profile_id} --- {item.handle}</div>
          })}
        </p>
      </p>
      <p>
        <h3>关注单个用户</h3>
        <button onClick={handleDoFollow}>doFollow</button>
        <p>注意：On-Chain: true 情况后端暂未开发完成</p>
      </p>
      <p>
        <h3>关注单个用户:handleDoFollowBroadCast</h3>
        <button onClick={handleDoFollowBroadCast}>doFollow</button>
      </p>
      <h3>关注单个用户---通过PostWithSig</h3>
      <button onClick={createRelationPostWithSig}>关注单个用户-abi</button>
      <br />
      <h3>关注单个用户---通过Post</h3>
      <button onClick={createRelationPost}>关注单个用户-abi</button>
      <br />


      <p>
        <h3>批量关注、取关</h3>
        <p>后端暂未开发完成</p>
        {/* addRelations 后端接口暂未实现 */}
      </p>
    </>
  )
}
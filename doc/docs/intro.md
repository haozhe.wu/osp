---
sidebar_position: 1
---

# 开发者文档

集成到您的app**只需简单几个步骤**



### 您需要满足以下环境

- [Node.js](https://nodejs.org/en/download/) 版本16.14或者以上。
- 钱包应用，推荐metamask，如果您使用chrome，请安装metamask的[浏览器扩展](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn)


## 开始集成sdk

目前我们提供了js sdk，支持react等框架app，未来还将支持react native、kotlin、flutter等框架的客户端sdk

[JS SDK](/docs/client/js/)

